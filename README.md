[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# TERCERA PRÁCTICA
## Java Message Service
Para la solución de la práctica se utilizará como herramienta de concurrencia JMS (Java Message Service). Se deben seguir las instrucciones de entrega para completar este proyecto. Esta práctica es una práctica en grupo de hasta dos alumnos y cada grupo deberá crear en el *broker* sus propios *destinos* para sus mensajes. Cada destino deberá definirse siguiendo la siguiente estructura:
```java
....
// En la interface Constantes del proyecto 
public static final String DESTINO =
 "ssccdd.curso2021.NOMBRE_GRUPO.BUZON";
...
```
El nombre del grupo tiene que ser único para los grupos, por lo que se recomienda usar alguna combinación de los nombres de usuario.

### Desarrollo de la práctica
 - **Instrucciones para la entrega:** En el espacio de docencia virtual de la asignatura hay definida una actividad donde se encuentran las instrucciones de entrega de la práctica. Esta es una práctica en grupo y para la entrega del proyecto de la práctica, se subirá el pdf con el enlace al tag de gitlab indicando además: los nombres, usuarios y grupos de ambos alumnos, en los grupos a los que pertenezcan los alumnos, si los dos alumnos pertenecen al mismo grupo solo hace falta que lo suba uno.
 - **Resolución teórica:** Consiste en realizar el análisis y diseño del problema. Para ello hay que especificar los TDAs (Clases) que sean necesarias, con sus atributos (estado) y sus métodos (comportamiento). Hay que justificar las decisiones que se tomen que no estén definidas en este enunciado. 
La documentación estará recogida en el fichero **README**, en formato [markdown](https://es.wikipedia.org/wiki/Markdown), en el repositorio de entrega. Un porcentaje de plagio mayor al 10% tendrá como consecuencia la no evaluación de la práctica.
 - **Implementación:** Para la implementación hay que utilizar las herramientas de concurrencia que nos proporciona JMS para el paso de mensajes. Para la ejecución de las tareas que conforman la práctica hay que utilizar el marco de ejecución [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) con alguna de las clases que implementan esta interfaces.
 - En el proyecto deberán crearse tres paquetes java:
	 - Un paquete para el lanzamiento del **Almacén Médico**
	 - Un paquete para el lanzamiento del **Centro Médico**
	 - Un paquete con las clases e interfaces comunes.
 - Para la prueba de ejecución cada uno de los miembros del grupo deberá ejecutar en su ordenador uno de los tipos de procesos, es decir, uno ejecutará los **almacenes** y el otro los **centros**.
El código se someterá también a la detección de copias y un porcentaje mayor del 10% tendrá como consecuencia la no evaluación de la práctica.

### PROBLEMA A RESOLVER
En un sistema de distribución regional de vacunas se ha decidido optar por un sistema de distribución según el ritmo de consumo de los distintos centros de médicos de vacunación. Los almacenes médicos empaquetaran lotes de vacunas y los pondrán a disposición de los distintos centros. Todo el proceso se realiza concurrentemente. Las características del proceso son:

 - Cada proceso almacén y centro tendrá un buzón propio para la comunicación directa entre ellos. Además habrá un buzón común de tipo subscripción donde los almacenes publicarán sus mensajes de disponibilidad de lotes de vacunas.
 - Los lotes de vacunas tendrán:
	 - Un identificador de lote, que será único en todo el sistema.
	 - Un fabricante: "Pfinos", "Antigua" o "AstroLunar". 
	 - El número de dosis incluidas en el lote, que variará entre 50 y 100 dosis. 
 - Los mensaje de publicación/solicitud de lote tendrán:
	 - Un identificador de lote ofrecido o solicitado, este identificador será negativo en caso de que el lote se hubiera entregado a otro centro.
	 - Un identificador de buzón que funcionará como remitente, para que los centros sepan donde solicitar el lote publicado y los almacenes sepan donde entregar el lote solicitado.
	 - Un lote de dosis de vacunas, esta variable solo será no nula en el mensaje de entrega de lote.
 - Los almacenes médicos ejecutarán la siguiente secuencia:
	 - Antes de empezar la ejecución se esperarán un tiempo aleatorio de al menos 5 segundos.
	 - Pasado ese tiempo se creará un lote de dosis aleatorio y publicará un mensaje en el buzón común con el id del lote y el buzòn propio para solicitarlo. **Nota**: el buzón será único para cada almacén y la variable lote será nula.
	 - Cuando pasen 5 segundos, si nadie a solicitado el lote, se volverá a publicar el mensaje. Si alguien ha solicitado el lote y se ha entregado, se genera uno nuevo y se publica.
	 - Durante toda la ejecución del almacén estará esperando de forma asíncrona mensajes de solicitud en su buzón, al primer centro que pida un lote devolvera el lote al buzón indicado en la solicitud. Si el lote ya ha sido servido, devolverá el mensaje con un id negativo.
	 - El almacén repetirá este proceso hasta ser interrumpido.
 - Los centros médicos ejecutarán la siguiente secuencia:
	 - Esperarán de forma síncrona un mensaje de oferta de lote en el buzón común. Cuando se reciba un mensaje, se contestará al buzón indicado dentro del mensaje, el contenido será el id del lote ofertado y el buzón propio.
	 - Cuando hayan enviado la petición de lote se esperará a la recepción de la contestación en el buzòn propio, cuando llegue la respuesta:
		 - Si el id de lote es válido, un entero positivo, se esperará 100 milisegundos por cada dosis.
		 - Si el id de lote es inválido, un entero negativo, se descarta el mensaje.
	 - Los centros realizarán esta secuencia hasta que hayan conseguido recibir 10 lotes.
- **Sistema**:
	 - Se construirán **NUM_ALMACENES** Almacenes médicos.
	 - Se construirán **NUM_CENTROS** Centros médicos.
	 - Los almacenes necesitarán algún método que permita generar identificadores de lotes únicos.
	 - El hilo principal que lanza a los almacenes estará ejecutándose hasta que reciba una indicación por teclado desde la consola, en ese momento se interrumpirán todos los almacenes y se imprimirá el número de lotes ofertados, cuantas veces ha ofertado un lote, cuantos lotes ha enviado, cuantas veces ha rechazado una solicitud y el detalle de todos los lotes generados.
	 - El alumno que ejecute a los centros médicos deberá imprimir al finalizar la información de cada centro, incluyendo el número de lotes pedidos y recibidos, el número de lotes pedidos pero no recibidos y el detalle de todos los lotes recibidos.

### Análisis
Para la resolución del problema necesitamos diferenciar los diferentes tipos de procesos y elementos que se usarán en el desarrollo, el problema se va a enfocar como un **Productor/Consumidor** modificado con paso de mensajes usando buzones. En este caso será necesario definir un enumerado donde se incluyen los tipos necesarios:
```
TDA Enum Fabricante { Pfinos, Antigua, Astrolunar }
```
Hace falta definir a la clase del elemento que representa a los lotes:
```
TDA LoteDosis
  Variables:
	 Int iD
	 Fabricante fabricante
	 Int numDosis
  Funciones:
	 Int getID()
	 Fabricante getFabricante()
	 Int getNumDosis()
```
Para representar el mensaje que se pasará en las comunicaciones, necesario la definición de la clase **MensajeLote**:
```
TDA MensajeLote
  Variables:
	 Int idLote
	 Buzon remitente
	 LoteDosis lote
  Funciones:
	 Int getIdLote()
	 Buzon getRemitente()
	 LoteDosis getLote()
```
Con la definición de `MensajeLote` podemos representar los diferentes tipos de mensaje que se van a mandar, mensaje de publicación, petición y respuesta. En los mensajes de respuesta negativa, no se da el lote, el identificador del lote será negativo y solo en los de respuesta positiva, el lote no será nulo.
Además serán necesario definir una serie de constantes que ayuda a la legibilidad del diseño de la solución:
```
MIN_DOSIS : Número mínimo de dosis que pueden almacenar cada lote
MAX_DOSIS : Número máximo de dosis que pueden almacenar cada lote
TIEMPO_ESPERA_ALMACEN : Tiempo de espera entre publicaciones de lotes
TIEMPO_ESPERA_CENTRO_DOSIS : Tiempo que tardan los centros en usar cada dosis
NUMERO_LOTES_A_RECIBIR : Lotes que recibirá el centro antes de parar la ejecución
ID_INVALIDO : Identificador de lote inválido, vale cualquier entero negativo
LOTE_INVALIDO : Para mandar objetos de tipo lote nulos 
```
Para la comunicación será necesario un buzón donde todos los almacenes publiquen la oferta de los lotes y donde los centros se subscribirán para recibir esas ofertas. Cada centro y cada almacén necesita un buzón propio, durante el desarrollo se accederá al buzón del proceso usando la variable buzonPropio. Los buzones seguirán el siguiente esquema:
```
BUZON_OFERTAS : "ssccdd.curso2021.alluque.distribución"
BUZON_CENTRO : "ssccdd.curso2021.alluque.centro" + iDCentro
BUZON_ALMACEN : "ssccdd.curso2021.alluque.almacen" + iDAlmacén
```
Los almacenes compartirán una variable protegida para la generación de IDs únicos para los lotes, no se incluye en el diseño para simplificar el pseudocódigo.

### Diseño
El pseudocódigo para los diferentes procesos es el siguiente:

Para los procesos `CentroMedico`:
```
Proceso CentroMedico  
Variables:
	 Int iD 			//Identificador del centro
	 Buzon buzonPropio 		//Buzon para recibir mensajes
	 Buzon buzonPub			//Buzón donde se envían las publicaciones de lotes
	 Vector<LoteDosis> lotes	//Lotes recibidos correctamente
	 
ejecucion_proceso { // Hilo de ejecución del proceso
	
	while( lotes.size() < NUMERO_LOTES_A_RECIBIR ) {
		//Esperamos a que haya un lote disponible
		mensajePub = buzonPub.recibirMensaje();
		
		//Cogemos el id y el remitente del mensaje para solicitar el lote
		mensajePeticion = MensajeLote( mensajePub.getIdLote(), buzonPropio, LOTE_INVALIDO );
		buzonPeticion = mensajePub.getRemitente();
		buzonPeticion.enviarMensaje( mensajePeticion );
		
		//Esperamos la respuesta del almacén
		mensajeRespuesta = buzonPropio.recibirMensaje();
		if( mensajeRespuesta.getIdLote() >= 0 ) {
			//Mensaje válido
			loteRecibido = mensajeRespuesta.getLote();
			lotes.add( loteRecibido );
			esperaAplicacionDosis( TIEMPO_ESPERA_CENTRO_DOSIS * lotes.getNumDosis() );
		}
	}
}
```
Para los procesos `AlmacenMedico`:
```
Proceso AlmacenMedico
Variables:
	 Int iD 			//Identificador del almacén
	 Buzon buzonPropio 		//Buzon para recibir mensajes
	 Buzon buzonPub			//Buzón donde se envían las publicaciones de lotes

ejecucion_proceso { // Hilo de ejecución del proceso
 
	esperaInicialAleatoria(); //Espera de al menos 5 segundos
	
	//Primer lote se publica fuera del while
	loteActual = generarLoteAleatorio( MIN_DOSIS, MAX_DOSIS );
	mensaje = MensajeLote( loteActual.getId(), buzonPropio, LOTE_INVALIDO );
	buzonPub.enviarMensaje( mensaje );
	inicioTemp = ahora(); //Se guarda el tiempo actual para contar hasta TIEMPO_ESPERA_ALMACEN
	
	while( interrupcionTeclado ) {
		//Tiempo restante, es necesario recalcular en cada iteración.
		tiempoRestante = ( inicioTemp + TIEMPO_ESPERA_ALMACEN ) - ahora(); 
		
		SELECT 	//Si recibimos petición
			RECEIVE mensajeSolicitud = buzonPropio.recibirMensaje() 
			if( loteActual != LOTE_INVALIDO && loteActual.getID() == mensajeSolicitud.getIdLote() ) {
			 //Hay lote disponible y coincide con el ofertado
				mensajeRespuesta = MensajeLote( loteActual.getId(), buzonPropio, loteActual);
				loteActual == LOTE_INVALIDO ;
			} else {
				mensajeRespuesta = MensajeLote( ID_INVALIDO, buzonPropio, LOTE_INVALIDO );
			}
			
			buzonRespuesta = mensajeSolicitud.getRemitente();
			buzonRespuesta.enviarMensaje( mensajeRespuesta );
			
		or	//Si pasa el tiempo lanzamos una nueva publicación
			TIMEOUT tiempoRestante //Alternativa: ELSE ahora() > inicioTemp + TIEMPO_ESPERA_ALMACEN
			if( loteActual == LOTE_INVALIDO ) { //Necesitamos un lote nuevo
				loteActual = generarLoteAleatorio( MIN_DOSIS, MAX_DOSIS );
			}
			mensaje = MensajeLote( loteActual.getId(), buzonPropio, LOTE_INVALIDO );
			buzonPub.enviarMensaje( mensaje );
			inicioTemp = ahora(); //Se guarda el nuevo tiempo inicial
	}
}
```
