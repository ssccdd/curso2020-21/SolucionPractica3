/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.almacen;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.LOTE_INVALIDO;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.QUEUE_PUBSUB;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.QUEUE_ALMACEN;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.NUMERO_MIN_DOSIS;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.NUMERO_MAX_DOSIS;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_INICIAL_ALMACEN_MIN;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_INICIAL_ALMACEN_MAX;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_ALMACEN;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.curso2021.practica3.comun.LoteDosis;
import es.uja.ssccdd.curso2021.practica3.comun.MensajeLote;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {

    private final int iD;
    private final AtomicInteger generadorIDs;
    private final String queuePropia;
    private int contadorPublicaciones;
    private final AtomicInteger contadorEnvios;
    private final AtomicInteger contadorRechazos;
    private final ArrayList<LoteDosis> lotesGenerados;
    private final HashMap<Integer, Integer> contadorPeticiones;
    private final ContenedorLote contenedor;

    //ActiveMQ
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private Destination destinationPropia;
    private MessageProducer producerPubSub;
    private MessageConsumer consumerListener;

    public AlmacenMedico(int iD, AtomicInteger generadorIDs) {
        this.iD = iD;
        this.generadorIDs = generadorIDs;
        this.queuePropia = QUEUE_ALMACEN + iD;;
        this.contadorPublicaciones = 0;
        this.contadorEnvios = new AtomicInteger(0);
        this.contadorRechazos = new AtomicInteger(0);
        this.lotesGenerados = new ArrayList<>();
        this.contadorPeticiones = new HashMap<>();
        this.contenedor = new ContenedorLote();//Para comunicar con el listener
    }

    @Override
    public void run() {
        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Almacén " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
            System.out.println(getInfoProceso());
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createTopic(QUEUE_PUBSUB);
        destinationPropia = session.createQueue(queuePropia);
        producerPubSub = session.createProducer(destination);

        //Limpia el buzon de mensajes antiguos
        MessageConsumer limpiador = session.createConsumer(destinationPropia);
        Message mensaje = limpiador.receive(10);
        while (mensaje != LOTE_INVALIDO) {
            mensaje = limpiador.receive(10);
        }
        limpiador.close();

//Listener
        consumerListener = session.createConsumer(destinationPropia);
        ListenerAlmacén listener = new ListenerAlmacén(iD, queuePropia, contadorEnvios, contadorRechazos, contadorPeticiones, contenedor, session);
        consumerListener.setMessageListener(listener);

    }

    private void task() throws JMSException {
        Gson gson = new GsonBuilder().create();
        boolean interrumpido = false;

        //Espera inicial
        try {
            TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextInt(TIEMPO_ESPERA_INICIAL_ALMACEN_MIN, TIEMPO_ESPERA_INICIAL_ALMACEN_MAX));
        } catch (InterruptedException ex) {
            //Interrumpido antes de empezar
            interrumpido = true;
        }

        System.out.println("Almacén médico " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                LoteDosis lote = contenedor.getLote();
                //Se tiene que crear un nuevo lote, porque se ha servido el anterior
                if (lote == LOTE_INVALIDO) {

                    int idLote = generadorIDs.getAndIncrement();
                    FabricanteVacuna fabricante = FabricanteVacuna.getFabricante(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                    int numDosis = ThreadLocalRandom.current().nextInt(NUMERO_MIN_DOSIS, NUMERO_MAX_DOSIS);
                    lote = new LoteDosis(idLote, fabricante, numDosis);

                    lotesGenerados.add(lote);
                    contadorPeticiones.put(idLote, 0);

                    //No hace falta proteger, el contenedor es seguro a accesos concurrentes
                    contenedor.setLote(lote);

                    System.out.println("Almacén médico " + iD + " ha generado el lote: " + lote.toString());

                }

                //PUBLICACIÓN DE LOTE
                MensajeLote peticionMQ = new MensajeLote(lote.getiD(), queuePropia);
                TextMessage mensajePeticion = session.createTextMessage(gson.toJson(peticionMQ));
                producerPubSub.send(mensajePeticion);
                contadorPublicaciones++;

                System.out.println("Almacén médico " + iD + " ha publicado el lote: " + lote.toString());

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ALMACEN);

            } catch (InterruptedException ex) {
                interrumpido = true;
            }
        }

    }

    public void after() {
        try {
            if (consumerListener != null) {
                consumerListener.close();
            }
            if (producerPubSub != null) {
                producerPubSub.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private String getInfoProceso() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nAlmacén médico ").append(iD);
        mensaje.append("\nPublicaciones realizadas: ").append(contadorPublicaciones);
        mensaje.append("\nPeticiones aceptadas: ").append(contadorEnvios);
        mensaje.append("\nPeticiones rechazadas: ").append(contadorRechazos);
        mensaje.append("\nLotes generados: ").append(lotesGenerados.size());

        mensaje.append("\nLotes creados:");
        for (LoteDosis lote : lotesGenerados) {
            mensaje.append("\n\t").append(lote.toString()).append(" pedido ").append(contadorPeticiones.get(lote.getiD())).append(" veces.");
        }

        mensaje.append("\n");

        return mensaje.toString();
    }

}
