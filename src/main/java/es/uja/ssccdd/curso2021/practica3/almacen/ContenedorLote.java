/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.almacen;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.LOTE_INVALIDO;
import es.uja.ssccdd.curso2021.practica3.comun.LoteDosis;

/**
 * Esta clase facilita el intercambio de lotes entre el almacén y el listener,
 * se usa synchronized para proteger el acceso simultáneo sin depender de locks
 * compartidos.
 *
 * @author Adrian Luque Luque (alluque)
 */
public class ContenedorLote {

    private LoteDosis lote;

    public ContenedorLote() {
        this.lote = LOTE_INVALIDO;
    }

    public synchronized LoteDosis getLote() {
        return lote;
    }

    public synchronized void setLote(LoteDosis lote) {
        this.lote = lote;
    }

    /**
     * *
     * Para poder comprobar si el lote es correcto, devolverlo y borrarlo de
     * forma protegida
     *
     * @param id id para comprobar si es correcto el lote
     * @return el lote si es no nulo y correcto, LOTE_INVALIDO en cualquier otro
     * caso.
     */
    public synchronized LoteDosis getLoteYBorraPorID(int id) {

        if (lote != LOTE_INVALIDO && id == lote.getiD()) {
            LoteDosis aux = lote;
            lote = LOTE_INVALIDO;
            return aux;
        }

        return LOTE_INVALIDO;
    }

}
