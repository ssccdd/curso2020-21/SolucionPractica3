/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.almacen;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.LOTE_INVALIDO;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.ID_INVALIDO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.curso2021.practica3.comun.LoteDosis;
import es.uja.ssccdd.curso2021.practica3.comun.MensajeLote;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class ListenerAlmacén implements MessageListener {

    private final int iDAlmacen;
    private final String queuePropia;
    private final AtomicInteger contadorEnvios;
    private final AtomicInteger contadorRechazos;
    private final HashMap<Integer, Integer> contadorPeticiones;
    private final ContenedorLote contenedor;
    private final Session session;

    public ListenerAlmacén(int iDAlmacen, String queuePropia, AtomicInteger contadorEnvios, AtomicInteger contadorRechazos, HashMap<Integer, Integer> contadorPeticiones, ContenedorLote contenedor, Session session) {
        this.iDAlmacen = iDAlmacen;
        this.queuePropia = queuePropia;
        this.contadorEnvios = contadorEnvios;
        this.contadorRechazos = contadorRechazos;
        this.contadorPeticiones = contadorPeticiones;
        this.contenedor = contenedor;
        this.session = session;
        System.out.println("Listener del almacén médico " + iDAlmacen + " iniciado.");
    }

    @Override
    public void onMessage(Message msg) {
        try {
            Gson gson = new GsonBuilder().create();
            MensajeLote peticion = gson.fromJson(((TextMessage) msg).getText(), MensajeLote.class);

            int idPedido = peticion.getiDLote();

            LoteDosis lote = contenedor.getLoteYBorraPorID(idPedido);
            //Si lote nulo, id negativo.
            int idLote = lote == LOTE_INVALIDO ? ID_INVALIDO : lote.getiD();

            //La queue podría ser nula porque no se espera respuesta a este mensaje
            MensajeLote respuesta = new MensajeLote(idLote, queuePropia, lote);

            Destination destinoRespuesta = session.createQueue(peticion.getRemitente());
            MessageProducer mpRespuesta = session.createProducer(destinoRespuesta);
            TextMessage mensajeRespuesta = session.createTextMessage(gson.toJson(respuesta));
            mpRespuesta.send(mensajeRespuesta);
            mpRespuesta.close();

            //Actualización de contadores
            if (idLote < 0) {
                contadorRechazos.getAndIncrement();
                System.out.println("Listener del almacén médico " + iDAlmacen + " ha rechazado la petición del lote: " + idPedido);
            } else {
                contadorEnvios.getAndIncrement();
                System.out.println("Listener del almacén médico " + iDAlmacen + " ha aceptado la petición del lote: " + idPedido);
            }

            //se comprueba si el lote ha sido generado por el almacen, se pueden recibir mensajes antiguos.
            if (contadorPeticiones.containsKey(idPedido)) {
                contadorPeticiones.replace(idPedido, contadorPeticiones.get(idPedido) + 1);
            }

        } catch (JMSException ex) {
            //No esperamos excepción, si ocurre se muestra en consola.
            Logger.getLogger(ListenerAlmacén.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
