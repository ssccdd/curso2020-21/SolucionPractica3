/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.almacen;


import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_CENTRO_DOSIS;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.ALMACENES_A_GENERAR;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MainAlmacen {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        AtomicInteger contIdLotes = new AtomicInteger();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando almacenes");
        for (int i = 0; i < ALMACENES_A_GENERAR; i++) {
            executor.execute(new AlmacenMedico(i, contIdLotes));
        }

        System.out.println("HILO-Principal Pulsa intro para parar los almacenes.");

        try {
            System.in.read();
        } catch (IOException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(MainAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        executor.shutdownNow();
        
        try {
            executor.awaitTermination(TIEMPO_ESPERA_CENTRO_DOSIS, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(MainAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        System.out.println("HILO-Principal Ha finalizado la ejecución");

    }

}
