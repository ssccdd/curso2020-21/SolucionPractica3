/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.centro;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.QUEUE_PUBSUB;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.QUEUE_CENTRO;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_CENTRO_DOSIS;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIMEOUT_PUBSUB;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIMEOUT_RESPUESTA;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.ID_INVALIDO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.curso2021.practica3.comun.LoteDosis;
import es.uja.ssccdd.curso2021.practica3.comun.MensajeLote;
import es.uja.ssccdd.curso2021.practica3.comun.SinContactoConAlmacénException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class CentroMedico implements Runnable {

    private final int iD;
    private final String queuePropia;
    private int contadorPedidos;
    private int contadorRecibidos;
    private final ArrayList<LoteDosis> lotesRecibidos;

    //ActiveMQ
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private Destination destinationPropia;
    private MessageConsumer consumerPubSub;

    public CentroMedico(int iD) {
        this.iD = iD;
        this.queuePropia = QUEUE_CENTRO + iD;
        this.contadorPedidos = 0;
        this.contadorRecibidos = 0;
        this.lotesRecibidos = new ArrayList<>();
    }

    @Override
    public void run() {
        try {
            before();
            task();
        } catch (SinContactoConAlmacénException e) {
            System.out.println("Centro " + iD + " ha perdido la conexión con los almacenes: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Centro " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
            System.out.println(getInfoProceso());
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createTopic(QUEUE_PUBSUB);
        destinationPropia = session.createQueue(queuePropia);
        consumerPubSub = session.createConsumer(destination);

        //Limpia el buzon de mensajes antiguos
        MessageConsumer limpiador = session.createConsumer(destinationPropia);
        Message mensaje = limpiador.receive(10);
        while (mensaje != null) {
            mensaje = limpiador.receive(10);
        }
        limpiador.close();

    }

    private void task() throws SinContactoConAlmacénException {
        Gson gson = new GsonBuilder().create();

        System.out.println("Centro médico " + iD + " ha empezado.");

        while (contadorRecibidos < 10) {

            try {
                //Esperamos 10 segundos, si no recibimmos respuesta suponemos que no hay almacenes y terminamos
                //Los timeout no son necesarios, pero queda mejor la solución final
                TextMessage mensajePub = (TextMessage) consumerPubSub.receive(TIMEOUT_PUBSUB);
                if (mensajePub == null) {
                    throw new SinContactoConAlmacénException("TimeOut en la subscripción.");
                }
                MensajeLote mensaje = gson.fromJson(mensajePub.getText(), MensajeLote.class);

                //Para depurar
                int idAlmacenOrigen = Integer.parseInt(mensaje.getRemitente().substring(mensaje.getRemitente().lastIndexOf("almacen") + 7));
                System.out.println("Centro " + iD + " recibe oferta del lote " + mensaje.getiDLote() + " del almacén " + idAlmacenOrigen);

                //PETICIÓN DE LOTE
                Destination destinoPeticion = session.createQueue(mensaje.getRemitente());
                MessageProducer mpPeticion = session.createProducer(destinoPeticion);
                MensajeLote peticionMQ = new MensajeLote(mensaje.getiDLote(), queuePropia);
                TextMessage mensajePeticion = session.createTextMessage(gson.toJson(peticionMQ));
                mpPeticion.send(mensajePeticion);
                contadorPedidos++;
                mpPeticion.close();

                //RECEPCIÓN RESPUESTA
                MessageConsumer mcPeticion = session.createConsumer(destinationPropia);
                TextMessage mensajeMQRespuesta = (TextMessage) mcPeticion.receive(TIMEOUT_RESPUESTA);
                if (mensajeMQRespuesta == null) {
                    mcPeticion.close();
                    throw new SinContactoConAlmacénException("TimeOut en la respuesta.");
                }
                MensajeLote mensajeRespuesta = gson.fromJson(mensajeMQRespuesta.getText(), MensajeLote.class);
                mcPeticion.close();

                //Tratamiento de la respuesta
                if (mensajeRespuesta.getiDLote() != ID_INVALIDO) {

                    LoteDosis lote = mensajeRespuesta.getLote();
                    System.out.println("Centro " + iD + " recibe respuesta positiva del lote " + lote.getiD()
                            + " con " + lote.getNumDosis() + " dosis de " + lote.getFabricante().name()
                            + " del almacén " + idAlmacenOrigen);
                    lotesRecibidos.add(lote);
                    contadorRecibidos++;

                    TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_CENTRO_DOSIS * lote.getNumDosis());

                } else {
                    System.out.println("Centro " + iD + " recibe respuesta negativa del lote "
                            + mensaje.getiDLote() + " del almacén " + idAlmacenOrigen);
                }

            } catch (InterruptedException | JMSException ex) {
                //No esperamos interrupción                
                Logger.getLogger(CentroMedico.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void after() {
        try {
            if (consumerPubSub != null) {
                consumerPubSub.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private String getInfoProceso() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nCentro médico").append(iD);
        mensaje.append("\nPedidos ").append(contadorPedidos).append(" lotes.");
        mensaje.append("\nRecibidos ").append(contadorRecibidos).append(" lotes.");
        mensaje.append("\nNo recibidos ").append(contadorPedidos - contadorRecibidos).append(" lotes.");

        mensaje.append("\nLotes recibidos:");
        for (LoteDosis lote : lotesRecibidos) {
            mensaje.append("\n\t").append(lote.toString());
        }

        mensaje.append("\n");

        return mensaje.toString();
    }

}
