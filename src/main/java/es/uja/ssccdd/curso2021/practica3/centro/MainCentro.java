/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.centro;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.TIEMPO_ESPERA_CENTRO_DOSIS;
import static es.uja.ssccdd.curso2021.practica3.comun.Utils.CENTROS_A_GENERAR;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MainCentro {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando centros");
        for (int i = 0; i < CENTROS_A_GENERAR; i++) {
            executor.execute(new CentroMedico(i));
        }

        System.out.println("HILO-Principal Espera a los centros");
        executor.shutdown();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_CENTRO_DOSIS, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(MainCentro.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

    }

}
