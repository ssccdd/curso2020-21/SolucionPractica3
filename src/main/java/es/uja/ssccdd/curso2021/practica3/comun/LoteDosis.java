/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.comun;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.FabricanteVacuna;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class LoteDosis {

    private final int iD;
    private final FabricanteVacuna fabricante;
    private final int numDosis;

    public LoteDosis(int iD, FabricanteVacuna fabricante, int numDosis) {
        this.iD = iD;
        this.fabricante = fabricante;
        this.numDosis = numDosis;
    }

    public FabricanteVacuna getFabricante() {
        return fabricante;
    }

    public int getiD() {
        return iD;
    }

    public int getNumDosis() {
        return numDosis;
    }

    @Override
    public String toString() {
        return "LoteDosis{" + "iD= " + iD + ", fabricante= " + fabricante + ", dosis= " + numDosis + "}";
    }

}
