/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.comun;

import static es.uja.ssccdd.curso2021.practica3.comun.Utils.LOTE_INVALIDO;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MensajeLote {

    private final int iDLote;
    private final String remitente;
    private LoteDosis lote;

    //Para los mensajes de oferta y petición
    public MensajeLote(int iDLoteOfertado, String remitente) {
        this.iDLote = iDLoteOfertado;
        this.remitente = remitente;
        this.lote = LOTE_INVALIDO;
    }

    //Para los mensajes donde se incluye el lote
    public MensajeLote(int iDLoteOfertado, String remitente, LoteDosis lote) {
        this.iDLote = iDLoteOfertado;
        this.remitente = remitente;
        this.lote = lote;
    }

    public LoteDosis getLote() {
        return lote;
    }

    public String getRemitente() {
        return remitente;
    }

    public int getiDLote() {
        return iDLote;
    }

    @Override
    public String toString() {
        return "MensajeLote{" + "Lote= " + lote.toString() + ", remitente= " + remitente + "}";
    }

}
