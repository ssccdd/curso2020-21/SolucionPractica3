/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.comun;

/**
 * Esta clase no es necesaria, pero permite una gestión de los timeOut similar a
 * la excepciones y genera código mas limpio.
 *
 * @author Adrian Luque Luque (alluque)
 */
public class SinContactoConAlmacénException extends Exception {

    public SinContactoConAlmacénException(String mensaje) {
        super(mensaje);
    }

}
