/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.practica3.comun;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static final String QUEUE_PUBSUB = "ssccdd.curso2021.alluque.distribución";
    public static final String QUEUE_ALMACEN = "ssccdd.curso2021.alluque.almacen";
    public static final String QUEUE_CENTRO = "ssccdd.curso2021.alluque.centro";
    public static final String BROKER_URL = "tcp://suleiman.ujaen.es:8018";

    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_FABRICANTES = FabricanteVacuna.values().length;
    public static final int TIEMPO_ESPERA_INICIAL_ALMACEN_MIN = 5000;
    public static final int TIEMPO_ESPERA_INICIAL_ALMACEN_MAX = 10000;
    public static final int TIEMPO_ESPERA_ALMACEN = 5000;
    public static final int TIEMPO_ESPERA_CENTRO_DOSIS = 100;
    public static final int NUMERO_LOTES_A_RECIBIR = 5;
    public static final int ALMACENES_A_GENERAR = 10;
    public static final int CENTROS_A_GENERAR = 5;
    public static final int NUMERO_MIN_DOSIS = 50;
    public static final int NUMERO_MAX_DOSIS = 101;//Valor Exclusivo
    public static final int TIMEOUT_PUBSUB = 10000;
    public static final int TIMEOUT_RESPUESTA = 10000;
    public static final LoteDosis LOTE_INVALIDO = null;
    public static final int ID_INVALIDO = -1;

    //Enumerado para el fabricante de la vacuna
    public enum FabricanteVacuna {
        PFINOS(33), ANTIGUA(66), ASTROLUNAR(100);

        private final int valor;

        private FabricanteVacuna(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un tipo de vacuna relacionada con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación
         * @return el laboratorio asociado con el valor de generación
         */
        public static FabricanteVacuna getFabricante(int valor) {
            FabricanteVacuna resultado = null;
            FabricanteVacuna[] laboratorios = FabricanteVacuna.values();
            int i = 0;

            while ((i < laboratorios.length) && (resultado == null)) {
                if (laboratorios[i].valor >= valor) {
                    resultado = laboratorios[i];
                }

                i++;
            }

            return resultado;
        }

        /**
         * Obtenemos un tipo de vacuna relacionada con su valor ordinal
         *
         * @param ordinal, entre 0 y (TOTAL_TIPOS_FABRICANTES - 1)
         * @return el laboratorio asociado con el valor ordinal
         */
        public static FabricanteVacuna getFabricantePorOrdinal(int ordinal) {
            return FabricanteVacuna.values()[ordinal];
        }
    }

}
